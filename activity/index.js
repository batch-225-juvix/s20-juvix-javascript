

/*
INSTRUCTIONS:

1.In the S20 folder, create an activity folder and an index.html and index.js file inside of it.
2.Link the index.js file to the index.html file.

3.Create a variable number that will store the value of the number provided by the user via the prompt.
4.Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
5.Create a condition that if the current value is less than or equal to 50, stop the loop.
6.Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
7.Create another condition that if the current value is divisible by 5, print the number.
8.Create a variable that will contain the string supercalifragilisticexpialidocious.
9.Create another variable that will store the consonants from the string.
10.Create another for Loop that will iterate through the individual letters of the string based on it’s length.
11.Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12.Create an else statement that will add the letter to the second variable.
13.Create a git repository named S20.
14.Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15.Add the link in Boodle.

*/


// ANSWER:

// 3.
/*let number = Number(prompt("give me a number"));

do {
	console.log("Do While: " + number);

	number += 1;
} while (number < 123)



// 4.
for (let count = 0; count <= 0; count++) {
	console.log(count)
}

// 5. (mag,sstop ang loop kapag less than 50 na.)
for (let loop = 60; loop >=50; loop--) {
	console.log(loop)
}

// 6.
for (let divisible = 10; divisible <=20; divisible++
	) {
	console.log(divisible)
}

*/
/*---------------------------------------------------------*/

function numFunct() {
    let numInput = Number(prompt("Enter you number:"))
    console.log("The number you provided is " + numInput);
do {
    let moduloTen = numInput % 10
    let moduloFive = numInput % 5
    if (moduloTen === 0) {
        console.log("The number is divisible by 10. Skipping a number")
    } else if (moduloFive === 0) {
        console.log(numInput)

    } else {
        console.log("n/a")
    }
    numInput -= 5
} while(numInput>50)    
}
numFunct();

function getConsonant() {
    let word = prompt("Enter your word!");
    let cons = "";
    for (let i=0; i<word.length; i++) {

        if (word[i].toLowerCase() === "a" || word[i].toLowerCase() === "e" || word[i].toLowerCase() === "i" || word[i].toLowerCase() === "o" || word[i].toLowerCase() === "u")

    {

    } else {
        cons += word[i]
    }
}
console.log(word);
console.log(cons);
}
getConsonant();