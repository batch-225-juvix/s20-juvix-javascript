// console.log("hellow");

// [SECTION] While Loop

// If the condition evaluates to true, the statements inside the code block will executed.

/*

	Syntax
	while (expression/condition){
		statement
	}
	
*/

// While the value of count is not equal to 0.

/*let count = 5;

while(count !== 0){
	console.log("While: " + count);

	// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5. Nag,plus plus lang siya.

	// ++ - increment, -- decrement

	count--;
}
*/

/*---------------------------------------*/
// [SECTION] Do While Loop

// A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.


/*

	Syntax:

	do{
		statement
	} while (expression/condition)

*/

let number = Number(prompt("give me a number"));

do {
	console.log("Do While: " + number);

	number += 1;
} while (number < 10)

/*----------------------------------------*/


// [SECTION] For Loops //

// A for loop is more flexible than while and do-while loops.

/*
	Syntax
	for (initialization; expression/condition; finalExpression) {
		statement
	}

*/




for (let count = 0; count <= 20; count++) {
	console.log(count);
}

/*naging endexing ng letter.*/

// Characters in strings may be counted using the .length property. (Kina,count niya yung ...)

let myString = "Alex";

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);


/*--------------------------------------*/
// Another example:
/*

let myString = prompt("Give me a word.")
console.log(myString.length)

*/
/*-------------------------------------*/

// ++ means iterate (dinadagdagan ulit).
// Will create a lopop that will print out the individual letters of a variable.

// Changing of vowels using loops.

for (let x = 0; x < myString.length; x++){

	// The current value of my string is printed out using it's index value.
	console.log(myString[x])
}
/*------------------------------------*/


// If the character of your name is a vowel letter, instead of displaying the character, display number "3".
let myName ="DelIZo"

for (let i = 0; i < myName.length; i++){
if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "u" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "o"
	){

	console.log(3);
} else {

	console.log(myName[i])
	}

}
/*-------------------------------------*/